import { AST } from "@typescript-eslint/typescript-estree";
import { BaseNode } from "estree";
import { WalkerHandler } from "estree-walker/types/sync";
import { Annotation, Loc } from './Annotator';

export type FileData<T> = {
  body: T,
  filename: string
}

export interface AnnotationHandler {
  enter: WalkerHandler;
  leave: WalkerHandler;
}

export type ExtendedNode = BaseNode & Annotation & Loc;
