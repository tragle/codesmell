import { Writable, WritableOptions } from 'stream';
import { Result } from './Collector';

const cycloAtLeast = (lowerBound: number) => (result: Result) => result.cyclomatic >= lowerBound;
const functionPrinter = (result: Result) => {
  const name = result.name || 'anonymous';
  const functionType = result.type;
  const location = `${result.filename}:${result.line},${result.column}`;
  const cyclo = result.cyclomatic;
  return `${functionType} "${name}", complexity: ${cyclo}\n${location}`;
}

interface ReporterOptions {
  complexityMin?: number;
}

class Reporter extends Writable {
  constructor(options?: WritableOptions & ReporterOptions) {
    super({ ...options, objectMode: true });
    this.complexityFilter = options && options.complexityMin ? cycloAtLeast(options.complexityMin) : cycloAtLeast(0);
  };

  complexityFilter: (result: Result) => boolean;

  _write(data: Result[], encoding: string, callback: () => void) {
    const reportData = data
      .filter(this.complexityFilter)
      .map(functionPrinter)
      .join('\n\n');
    console.log(reportData);
  }
}

export default Reporter;
