import { Transform, TransformOptions } from 'stream';
import { AST, parse } from "@typescript-eslint/typescript-estree";
import { FileData } from "./types";

class Parser extends Transform {
  constructor(options?: TransformOptions) {
    super({ ...options, objectMode: true });
  }

  _transform(data: FileData<string>, encoding: string, callback: () => void) {
    try {
      const ast = parse(data.body, {
        loc: true,
        range: true,
        jsx: true
      });
      this.push({
        body: ast,
        filename: data.filename
      });
    } catch (e) {
      console.error(e);
    }

    callback()
  }

};

export default Parser;
