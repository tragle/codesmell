import { Transform, TransformOptions } from 'stream';
import { FileData } from './types';
import { Result } from './Collector';

class ResultsList extends Transform {
  constructor(options?: TransformOptions ) {
    super({ ...options, objectMode: true });
    this.results = [];
  }

  results: Result[];

  _transform(data: FileData<Result>, encoding: string, callback: () => void) {
    this.results.push(data.body);
    callback();
  }

  _final(callback: () => void) {
    this.results.sort((left: Result, right: Result) => right.cyclomatic - left.cyclomatic);
    this.push(this.results);
  }
}

export default ResultsList;
