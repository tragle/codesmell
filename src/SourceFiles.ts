import readdir = require("recursive-readdir");
import { Readable, ReadableOptions } from 'stream';
import { readFileSync } from "fs";

interface SourceFilesOptions {
  path: string;
}

const isSourceFile = (file: string) => 
  file.endsWith(".ts") || file.endsWith(".tsx") || file.endsWith(".js") || file.endsWith(".jsx");


class SourceFiles extends Readable {
  constructor(options?: ReadableOptions & SourceFilesOptions) {
    super({ ...options, objectMode: true });
    this.path = options && options.path ? options.path : ".";
  }

  path: string

  _read() {
    readdir(this.path,  ["build", "node_modules", "*.json", "*.html"], (err, files) => {
      if (!err) {
        files.forEach(
          (file) => {
            if (isSourceFile(file) && this.readable) {
              const buffer = readFileSync(file);
              this.push({ 
                body: String(buffer),
                filename: file
              });
            }
          });
        this.push(null);
      } else {
        this.destroy(err);
      }
    });
  }
}

export default SourceFiles;
