/*
import { ArrowFunctionExpression, BaseNode, ClassDeclaration, FunctionDeclaration, Identifier, ImportDeclaration, VariableDeclarator } from "estree";
import { ExtendedNode } from './types';
import { AnnotationHandler } from "./types";

const isFunction = (node: BaseNode) => 
  node.type == "FunctionDeclaration" ||
    node.type == "FunctionExpression" ||
    node.type == "ArrowFunctionExpression";

const isBranch = (node: BaseNode) =>[
  "CatchClause",
  "ConditionalExpression",
  "DoWhileStatement",
  "ForInStatement",
  "ForOfStatement",
  "ForStatement",
  "IfStatement",
  "LogicalExpression",
  "SwitchCase",
  "WhileStatement",
].includes(node.type);

class ComplexityAnnotator implements AnnotationHandler {
  constructor() {
    this.fns = [];
  }

  fns: number[];

  enter (node: BaseNode) {
    if (isFunction(node)) {
      this.fns.push(1);
    }
    if (isBranch(node)) {
      if (this.fns.length) {
        this.fns[this.fns.length - 1]++;
      }
    }
  }

  leave (node: BaseNode) {
    if (isFunction(node)) {
      const cyclo = this.fns.pop() || 1;
      (node as ExtendedNode).cyclomatic = cyclo;
    }
  }

}

export default ComplexityAnnotator;
*/
