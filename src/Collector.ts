import { Transform, TransformOptions } from 'stream';
import { AST } from "@typescript-eslint/typescript-estree";
import { ExtendedNode } from './types';
import { BaseNode } from "estree";
import { walk } from "estree-walker";
import { SyncWalker } from "estree-walker/types/sync";
import { FileData } from "./types";

type CollectorHandler = (node: ExtendedNode) => void;

type CollectorHandlers = {
  enter: CollectorHandler;
  leave: CollectorHandler;
}

type CollectorWalker = (handlers: CollectorHandlers, ast: BaseNode | ExtendedNode) => void;

export type Result = {
  filename: string,
  line: number,
  column: number,
  type: string,
  cyclomatic: number,
  name: string | null,
};

function isExtended(node: BaseNode | ExtendedNode): node is ExtendedNode {
  return (node as ExtendedNode).cyclomatic !== undefined && node.loc !== undefined;
}

const collectResults: CollectorWalker = (handlers: CollectorHandlers, ast: BaseNode | ExtendedNode) => {
  const _ast = { ...ast };
  walk(_ast, (handlers as unknown) as SyncWalker);
}

class Collector extends Transform {
  constructor(options?: TransformOptions){
    super({ ...options, objectMode: true });
  }
  
  _transform(data: FileData<AST<{}>>, encoding: string, callback: () => void) {

    const results: Result[] = [];
    const handlers: CollectorHandlers = {
      enter: (node) => {
        if (isExtended(node)) {
          results.push({
            filename: data.filename,
            line: node.loc.start.line,
            column: node.loc.start.column,
            type: node.type,
            cyclomatic: node.cyclomatic,
            name: node.name || null
          });
        }
      },
      leave: (node) => {}
    }

    collectResults(handlers, data.body);

    results.forEach((result: Result) => {
      this.push({
        filename: data.filename,
        body: result
      })
    });

    callback();
  }
}

export default Collector;
