import { Writable, WritableOptions } from 'stream';
import { FileData } from './types';
import { Result } from './Collector';

class ObjectPrinter extends Writable {
  constructor(options?: WritableOptions ) {
    super({ ...options, objectMode: true });
    this.resultSets = [];
  }
  resultSets: Result[][];

  _write(data: Result[], encoding: string, callback: () => void) {
    this.resultSets.push(data);
    callback();
  }

  _final(callback: () => void) {
    this.resultSets.forEach((resultSet) => {
      resultSet.sort((left: Result, right: Result) => right.cyclomatic - left.cyclomatic);
      console.log(JSON.stringify(resultSet, null, 2));
    });
  }
}

export default ObjectPrinter;
