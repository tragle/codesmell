import { Transform, TransformOptions } from 'stream';
import { ArrowFunctionExpression, BaseNode, ClassDeclaration, FunctionDeclaration, Identifier, ImportDeclaration, VariableDeclarator } from "estree";
import { walk } from "estree-walker";
import { SyncWalker, WalkerHandler } from "estree-walker/types/sync";
import { AST } from "@typescript-eslint/typescript-estree";
import { FileData, ExtendedNode } from "./types";

type AnnotationHandlers = {
  enter: WalkerHandler;
  leave: WalkerHandler;
}

export type Loc = {
  loc: {
    start: {
      line: number,
      column: number
    },
    end: {
      line: number,
      column: number
    }
  },
}

export type Annotation = {
  cyclomatic: number,
  name?: string
}

const isFunction = (node: BaseNode) =>
  node.type == "FunctionDeclaration" ||
    node.type == "FunctionExpression" ||
    node.type == "ArrowFunctionExpression";

const isBranch = (node: BaseNode) =>[
  "CatchClause",
  "ConditionalExpression",
  "DoWhileStatement",
  "ForInStatement",
  "ForOfStatement",
  "ForStatement",
  "IfStatement",
  "LogicalExpression",
  "SwitchCase",
  "WhileStatement",
].includes(node.type);

const annotateAST = (handlers: AnnotationHandlers, ast: AST<{}>) => {
  const _ast = { ...ast };
  walk(_ast, handlers);
  return _ast;
}

const isFunctionAssignment = (node: BaseNode) => [
  "MethodDefinition",
  "VariableDeclarator",
].includes(node.type);


class Annotator extends Transform {
  constructor(options?: TransformOptions) {
    super({ ...options, objectMode: true });
  }

  _transform(data: FileData<AST<{}>>, encoding: string, callback: () => void) {

    const fns: number[] = [];

    const handlers: AnnotationHandlers = {
      enter: (node, parent, prop, index) => {
        if (isFunction(node)) {
          fns.push(1);
          if (node && (node as FunctionDeclaration).id) {
            (node as ExtendedNode).name = ((node as FunctionDeclaration).id as Identifier).name;
          } else if (isFunctionAssignment(parent) && (parent as FunctionDeclaration).id) {
            (node as ExtendedNode).name = ((parent as FunctionDeclaration).id as Identifier).name;
          }
        }
        if (isBranch(node)) {
          if (fns.length) {
            fns[fns.length - 1]++;
          }
        }
      },

      leave: (node, parent, prop, index) => {
        if (isFunction(node)) {
          const cyclo = fns.pop() || 1;
          (node as ExtendedNode).cyclomatic = cyclo;
        }
      },
    };

    const annotatedAST = annotateAST(handlers, data.body);

    this.push({
      body: annotatedAST,
      filename: data.filename
    });

    callback();
  }
}

export default Annotator;
