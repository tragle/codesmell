import parseArgs from 'minimist';
import FileStream from './SourceFiles';
import Parser from './Parser';
import Annotator from './Annotator';
import Collector from './Collector';
import ResultsList from './ResultsList';
import Reporter from './Reporter';

const args = parseArgs(process.argv.slice(2));

if (args.help || args.h) {
  console.log("Usage: codesmell [options] path\n")
  console.log("Options:"); 
  console.log("--complexity=N", "\t\tminimum cyclomatic complexity");
  console.log("--help, -h", "\t\tprint this message");
  
  process.exit(0);
}

const complexityMin = args.complexity || 0;
const path = args._.length ? args._[0] : ".";

const filestream = new FileStream({ path });
const parser = new Parser();
const annotator = new Annotator();
const collector = new Collector();
const results = new ResultsList();
const reporter = new Reporter({ complexityMin });
const errors = [];

try {
  filestream
    .pipe(parser)
    .pipe(annotator)
    .pipe(collector)
    .pipe(results)
    .pipe(reporter);
} catch (e) {
  errors.push(e);
}

errors.forEach((error) => { console.error(error); });

